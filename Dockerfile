# Build Container compiles everything into a binary
FROM golang:alpine as builder

RUN apk update && apk upgrade && \
    apk add --no-cache git

RUN mkdir /app
WORKDIR /app

ENV GO111MODULE=on

COPY . .

RUN go mod download
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o shippy-consignment-service

# Run container -- Takes stuff built from build container and puts those in smaller container of compiled binary -- without runtime. 
FROM alpine:latest

RUN apk --no-cache add ca-certificates

RUN mkdir /app
WORKDIR /app
COPY --from=builder /app/shippy-consignment-service .

CMD ["./shippy-consignment-service"]